import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'
import './assets/font-awesome-4.7.0/css/font-awesome.min.css'
import 'bootstrap/dist/css/bootstrap.css'
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle.js'

createApp(App).use(bootstrap).mount('#app')
